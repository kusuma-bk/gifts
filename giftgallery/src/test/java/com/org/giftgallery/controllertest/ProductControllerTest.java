package com.org.giftgallery.controllertest;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import com.org.giftgallery.controller.CartController;
import com.org.giftgallery.controller.ProductController;
import com.org.giftgallery.dto.AddCartResponseDto;
import com.org.giftgallery.dto.AddProductToCartDto;
import com.org.giftgallery.dto.ProductListResponseDto;
import com.org.giftgallery.dto.ProductResponseDto;
import com.org.giftgallery.dto.RecipientDto;
import com.org.giftgallery.exception.CartException;
import com.org.giftgallery.exception.CorporateException;
import com.org.giftgallery.exception.PersonalException;
import com.org.giftgallery.exception.ProductException;
import com.org.giftgallery.exception.QuantityException;
import com.org.giftgallery.exception.UserExistException;
import com.org.giftgallery.service.CartService;
import com.org.giftgallery.service.ProductService;

@RunWith(MockitoJUnitRunner.class)
public class ProductControllerTest {
	@Mock
	ProductService productService;
	@InjectMocks
	ProductController productController;
	@Mock
	CartService cartService;
	@InjectMocks
	CartController cartController;
	ProductListResponseDto productListResponseDto;
	ProductResponseDto productResponseDto;
	List<ProductResponseDto> productResponseDtos;
	AddProductToCartDto addProductToCartDto;
	RecipientDto recipientDto;
	List<RecipientDto> recipientDtos;
	AddCartResponseDto addCartResponseDto;

	@Before
	public void setup() {

		productResponseDto = new ProductResponseDto();
		productResponseDto.setPricePerProduct(100);
		productResponseDto.setProductDescription("elsectronics");
		productResponseDto.setProductId(1);
		productResponseDto.setProductName("laptop");
		productResponseDto.setQuantity(4);

		productResponseDtos = new ArrayList<>();
		productResponseDtos.add(productResponseDto);

		productListResponseDto = new ProductListResponseDto();
		productListResponseDto.setProductResponseDtos(productResponseDtos);
		productListResponseDto.setStatusCode(700);

		recipientDtos = new ArrayList<>();
		recipientDto = new RecipientDto();
		recipientDto.setMessage("hello");
		recipientDto.setRecipientEmail("abc@gmail.com");
		recipientDto.setRecipientName("abc");
		recipientDto.setRecipientPhoneNumber("43643643");

		addProductToCartDto = new AddProductToCartDto();
		addProductToCartDto.setProductId(1);
		addProductToCartDto.setQuantity(2);
		addProductToCartDto.setUserId(1);
		addProductToCartDto.setUserType("personal");
		addProductToCartDto.setRecipientDtos(recipientDtos);

		addCartResponseDto = new AddCartResponseDto();
		addCartResponseDto.setCartId(1);
		addCartResponseDto.setMessage("hii");
		addCartResponseDto.setStatuscode(202);
	}

	@Test
	public void listOfProducts() throws ProductException {

		Mockito.when(productService.productList()).thenReturn(productListResponseDto);
		ResponseEntity<ProductListResponseDto> response = productController.productsList();
		Assert.assertEquals(202, response.getStatusCodeValue());

	}

	@Test
	public void addToCart()
			throws QuantityException, CartException, UserExistException, CorporateException, PersonalException {
		Mockito.when(cartService.addToCard(addProductToCartDto)).thenReturn(addCartResponseDto);
		ResponseEntity<AddCartResponseDto> response = cartController.addToCart(addProductToCartDto);
		Assert.assertEquals(202, response.getStatusCodeValue());
	}
}
