package com.org.giftgallery.controllertest;

import javax.mail.MessagingException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.org.giftgallery.controller.OrderController;
import com.org.giftgallery.dto.OrderResponseDto;
import com.org.giftgallery.exception.CustemerTypeExceptionForCorperator;
import com.org.giftgallery.exception.CustemerTypeExceptionForPersonal;
import com.org.giftgallery.exception.OrderListNotPresent;
import com.org.giftgallery.service.OrderService;

@RunWith(MockitoJUnitRunner.class)
public class ControllerTest {
	@Mock
	OrderService orderService;

	@InjectMocks
	OrderController orderController;

	OrderResponseDto orderResponseDto;

	@Before
	public void setup() {
		orderResponseDto = new OrderResponseDto();
		orderResponseDto.setMessage("your order is comfirmed");
		orderResponseDto.setStatus(613);

	}

	@Test
	public void orderTest()
			throws CustemerTypeExceptionForPersonal, OrderListNotPresent, CustemerTypeExceptionForCorperator,
			MessagingException {

		Mockito.when(orderService.order(Mockito.anyLong())).thenReturn(orderResponseDto);

		OrderResponseDto response = orderService.order(Mockito.anyLong());
		Assert.assertEquals("your order is comfirmed", response.getMessage());

	}

}
