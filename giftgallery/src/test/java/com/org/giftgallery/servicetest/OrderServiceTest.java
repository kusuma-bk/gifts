package com.org.giftgallery.servicetest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.mail.MessagingException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.org.giftgallery.dto.OrderResponseDto;
import com.org.giftgallery.entity.Cart;
import com.org.giftgallery.entity.Recipient;
import com.org.giftgallery.entity.User;
import com.org.giftgallery.exception.CustemerTypeExceptionForCorperator;
import com.org.giftgallery.exception.CustemerTypeExceptionForPersonal;
import com.org.giftgallery.exception.OrderListNotPresent;
import com.org.giftgallery.repository.CartRepository;
import com.org.giftgallery.repository.OrderRepository;
import com.org.giftgallery.repository.RecipientRepository;
import com.org.giftgallery.repository.UserRepository;
import com.org.giftgallery.service.OrderServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class OrderServiceTest {
	@Mock
	OrderRepository orderRepository;

	@Mock
	RecipientRepository recipientRepository;

	@Mock
	CartRepository cartRepository;

	@Mock
	UserRepository userRepository;

	@InjectMocks
	OrderServiceImpl orderServiceImpl;

	List<Recipient> listOfRecipient, listOfRecipient1, listOfRecipient2;
	Recipient recipient;
	Cart cart, cart1;
	User user;

	@Before
	public void setup() {
		listOfRecipient = new ArrayList<>();
		listOfRecipient1 = new ArrayList<>();
		listOfRecipient2 = new ArrayList<>();

		recipient = new Recipient();
		cart = new Cart();
		cart1 = new Cart();

		recipient.setCartId(1);
		recipient.setMessage("all the best");
		recipient.setRecipientEmail("suppi@gmail.com");
		recipient.setRecipientId(1);
		recipient.setRecipientName("suppi");
		recipient.setRecipientPhoneNumber("9591881651");
		listOfRecipient.add(recipient);

		for (int i = 0; i < 5; i++) {
			listOfRecipient1.add(recipient);
		}

		cart.setCartId(1);
		cart.setProductId(1);
		cart.setQuantity(3);
		cart.setUserId(1);
		cart.setUserType("personal");

		cart1.setCartId(1);
		cart1.setProductId(1);
		cart1.setQuantity(3);
		cart1.setUserId(1);
		cart1.setUserType("corperate");

		user = new User();
		user.setEmailId("kusumabk112019@gmail.com");
		user.setPhoneNumber("98765432");
		user.setUserId(1l);
		user.setUserName("kusuma");
	}

	@Test
	public void orderTest() throws CustemerTypeExceptionForPersonal, OrderListNotPresent,
			CustemerTypeExceptionForCorperator, MessagingException {
		Mockito.when(recipientRepository.findByCartId(Mockito.anyLong())).thenReturn(listOfRecipient);
		Mockito.when(cartRepository.findByCartId(Mockito.anyLong())).thenReturn(Optional.of(cart));
		OrderResponseDto response = orderServiceImpl.order(Mockito.anyLong());
		Assert.assertNotNull(response);
		Assert.assertEquals(613, response.getStatus());

	}

	@Test(expected = OrderListNotPresent.class)
	public void orderOrderListNotPresentException() throws CustemerTypeExceptionForPersonal, OrderListNotPresent,
			CustemerTypeExceptionForCorperator, MessagingException {
		Mockito.when(recipientRepository.findByCartId(Mockito.anyLong())).thenReturn(listOfRecipient);
		Mockito.when(cartRepository.findByCartId(Mockito.anyLong())).thenReturn(Optional.empty());
		orderServiceImpl.order(Mockito.anyLong());

	}
	@Test(expected = CustemerTypeExceptionForPersonal.class)
	public void orderCustemerTypeExceptionForPersonaltException() throws CustemerTypeExceptionForPersonal,
			OrderListNotPresent, CustemerTypeExceptionForCorperator, MessagingException {
		Mockito.when(recipientRepository.findByCartId(Mockito.anyLong())).thenReturn(listOfRecipient1);
		Mockito.when(cartRepository.findByCartId(Mockito.anyLong())).thenReturn(Optional.of(cart));
		orderServiceImpl.order(Mockito.anyLong());

	}

	@Test(expected = CustemerTypeExceptionForCorperator.class)
	public void orderCustemerTypeExceptionForCorperatorException() throws CustemerTypeExceptionForPersonal,
			OrderListNotPresent, CustemerTypeExceptionForCorperator, MessagingException {
		Mockito.when(recipientRepository.findByCartId(Mockito.anyLong())).thenReturn(listOfRecipient);
		Mockito.when(cartRepository.findByCartId(Mockito.anyLong())).thenReturn(Optional.of(cart1));
		orderServiceImpl.order(Mockito.anyLong());

	}
	@Test
	public void sendMailTest() throws MessagingException {
		Mockito.when(cartRepository.findById(1l)).thenReturn(Optional.of(cart));
		Mockito.when(recipientRepository.findByCartId(1l)).thenReturn(listOfRecipient);
		Mockito.when(userRepository.findById(1l)).thenReturn(Optional.of(user));
		String response = orderServiceImpl.sendMail(1l);
		Assert.assertEquals("Mail Sent Success!", response);
	}

	@Test(expected = MessagingException.class)
	public void sendMailTestMessagingException() throws MessagingException {
		Mockito.when(cartRepository.findById(1l)).thenReturn(Optional.of(cart));
		Mockito.when(recipientRepository.findByCartId(1l)).thenReturn(listOfRecipient2);
		Mockito.when(userRepository.findById(1l)).thenReturn(Optional.of(user));
		String response = orderServiceImpl.sendMail(1l);
		Assert.assertEquals("Mail Sent Success!", response);
	}
}
