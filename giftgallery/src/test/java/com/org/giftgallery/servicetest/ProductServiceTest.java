package com.org.giftgallery.servicetest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.org.giftgallery.dto.AddCartResponseDto;
import com.org.giftgallery.dto.AddProductToCartDto;
import com.org.giftgallery.dto.ProductListResponseDto;
import com.org.giftgallery.dto.ProductResponseDto;
import com.org.giftgallery.dto.RecipientDto;
import com.org.giftgallery.entity.Cart;
import com.org.giftgallery.entity.Product;
import com.org.giftgallery.entity.Recipient;
import com.org.giftgallery.entity.User;
import com.org.giftgallery.exception.CartException;
import com.org.giftgallery.exception.CorporateException;
import com.org.giftgallery.exception.PersonalException;
import com.org.giftgallery.exception.ProductException;
import com.org.giftgallery.exception.QuantityException;
import com.org.giftgallery.exception.UserExistException;
import com.org.giftgallery.repository.CartRepository;
import com.org.giftgallery.repository.ProductRepository;
import com.org.giftgallery.repository.RecipientRepository;
import com.org.giftgallery.repository.UserRepository;
import com.org.giftgallery.service.CartServiceImpl;
import com.org.giftgallery.service.ProductServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class ProductServiceTest {
	@InjectMocks
	ProductServiceImpl productServiceImpl;
	@Mock
	ProductRepository productRepositry;
	@Mock
	CartRepository cartRepositry;
	@Mock
	UserRepository userRepositry;
	@InjectMocks
	CartServiceImpl cartServiceImpl;
	@Mock
	RecipientRepository recipientRepositry;
	Product product;
	List<Product> products;
	ProductListResponseDto productListResponseDto;
	ProductResponseDto productResponseDto;
	List<ProductResponseDto> productResponseDtos;
	User user;
	Cart cart;

	Recipient recipient;
	List<Recipient> recipients;
	AddProductToCartDto addProductToCartDto;
	RecipientDto recipientDto;
	List<RecipientDto> recipientDtos;
	AddCartResponseDto addCartResponseDto;
	Cart cart2;

	@Before
	public void setup() {
		addCartResponseDto = new AddCartResponseDto();
		addCartResponseDto.setCartId(1);
		addCartResponseDto.setMessage("hello");
		addCartResponseDto.setStatuscode(700);

		recipientDto = new RecipientDto();
		recipientDto.setMessage("hello");
		recipientDto.setRecipientEmail("bcd@gmail.com");
		recipientDto.setRecipientName("bcd");
		recipientDto.setRecipientPhoneNumber("325325325");

		recipientDtos = new ArrayList<>();
		recipientDtos.add(recipientDto);

		addProductToCartDto = new AddProductToCartDto();
		addProductToCartDto.setProductId(1);
		addProductToCartDto.setQuantity(3);
		addProductToCartDto.setRecipientDtos(recipientDtos);

		recipient = new Recipient();
		recipient.setCartId(1);
		recipient.setMessage("hello");
		recipient.setRecipientEmail("bcd@gmail.com");
		recipient.setRecipientId(2);
		recipient.setRecipientName("bcd");
		recipient.setRecipientPhoneNumber("4365777");

		user = new User();
		user.setEmailId("abc@gmail.com");
		user.setPhoneNumber("43643755");
		user.setUserId(1);
		user.setUserName("abc");

		productResponseDto = new ProductResponseDto();
		productResponseDto.setPricePerProduct(1000);
		productResponseDto.setProductDescription("electronics");
		productResponseDto.setProductId(1);
		productResponseDto.setProductName("lapy");
		productResponseDto.setQuantity(3);

		productResponseDtos = new ArrayList<>();
		productResponseDtos.add(productResponseDto);
		productListResponseDto = new ProductListResponseDto();
		productListResponseDto.setProductResponseDtos(productResponseDtos);
		productListResponseDto.setStatusCode(100);

		product = new Product();
		product.setPricePerProduct(1000);
		product.setProductDescription("electronics");
		product.setProductId(1);
		product.setProductName("lapy");
		product.setQuantity(4);

		products = new ArrayList<>();
		products.add(product);

		cart = new Cart();
		cart.setProductId(1);
		cart.setQuantity(3);
		cart.setUserId(1);
		cart.setUserType("personal");

		cart2 = new Cart();
	}

	@Test
	public void productList() throws ProductException {
		Mockito.when(productRepositry.findAll()).thenReturn(products);
		ProductListResponseDto ProductListResponseDto = productServiceImpl.productList();
		Assert.assertEquals(607, ProductListResponseDto.getStatusCode());
	}

	@Test(expected = ProductException.class)
	public void listproductException() throws QuantityException, CartException, UserExistException, PersonalException,
			CorporateException, ProductException {
		Mockito.when(productRepositry.findAll()).thenReturn(null);
		productServiceImpl.productList();
	}

	@Test(expected = UserExistException.class)
	public void addToCartuserExistException()
			throws QuantityException, CartException, UserExistException, PersonalException, CorporateException {
		Mockito.when(userRepositry.findByUserId(Mockito.anyLong())).thenReturn(null);
		cartServiceImpl.addToCard(addProductToCartDto);
	}

}
