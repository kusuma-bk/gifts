package com.org.giftgallery.servicetest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.org.giftgallery.dto.CartEditRequestDto;
import com.org.giftgallery.dto.CartResponseDto;
import com.org.giftgallery.dto.EditResponseDto;
import com.org.giftgallery.dto.RecipientRequestListDto;
import com.org.giftgallery.entity.Cart;
import com.org.giftgallery.entity.Product;
import com.org.giftgallery.entity.Recipient;
import com.org.giftgallery.entity.User;
import com.org.giftgallery.repository.CartRepository;
import com.org.giftgallery.repository.ProductRepository;
import com.org.giftgallery.repository.RecipientRepository;
import com.org.giftgallery.service.CartServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class CartServiceTest {
	@Mock
	CartRepository cartRepository;

	@Mock
	ProductRepository productRepository;

	@Mock
	RecipientRepository recipientRepository;

	@InjectMocks
	CartServiceImpl cartServiceImpl;

	CartResponseDto cartResponseDto;
	CartEditRequestDto cartEditRequestDto;
	EditResponseDto editResponseDto;
	Cart cart;
	Product product;
	User user;
	List<Recipient> recipientList;
	List<RecipientRequestListDto> listOfrecipients;
	Recipient recipient;
	Recipient recipientIn;
	RecipientRequestListDto recipientRequestListDto;
	long userId;

	@Before
	public void setUp() {
		userId = 1l;
		recipientList = new ArrayList<Recipient>();

		user = new User();
		user.setUserId(1);
		user.setEmailId("pallavi@gmail.com");
		user.setUserName("pallavi");
		user.setPhoneNumber("945856758");

		product = new Product();
		product.setProductId(1);
		product.setProductName("coffee mug");
		product.setQuantity(5);
		product.setProductDescription("coffee mug");
		product.setPricePerProduct(300);

		cart = new Cart();
		cart.setCartId(1);
		cart.setProductId(1);
		cart.setQuantity(5);
		cart.setUserId(1);
		cart.setUserType("corporate");

		recipientRequestListDto = new RecipientRequestListDto();
		recipientRequestListDto.setRecipientName("kusuma");
		recipientRequestListDto.setRecipientEmail("kusuma@gmail.com");
		recipientRequestListDto.setRecipientId(1);
		recipientRequestListDto.setRecipientPhoneNumber("48957575");
		recipientRequestListDto.setMessage("greetings from pallavi");

		listOfrecipients = new ArrayList<RecipientRequestListDto>();
		listOfrecipients.add(recipientRequestListDto);

		cartEditRequestDto = new CartEditRequestDto();
		cartEditRequestDto.setCartId(1);
		cartEditRequestDto.setQuantity(4);
		cartEditRequestDto.setRecipientDetails(listOfrecipients);

		editResponseDto = new EditResponseDto();
		editResponseDto.setMessage("updated success");
		editResponseDto.setStatus(680);

		recipient = new Recipient();
		recipient.setCartId(1);
		recipient.setRecipientName("uday");
		recipient.setRecipientId(2);
		recipient.setRecipientEmail("uday@gmail.com");
		recipient.setRecipientPhoneNumber("9476786496");
		recipient.setMessage("uday greetings from pallavi");

		recipientList.add(recipient);

		recipientIn = new Recipient();
		recipientIn.setCartId(1);
		recipientIn.setRecipientName("Laveti uday");
		recipientIn.setRecipientId(2);
		recipientIn.setRecipientEmail("l_uday@gmail.com");
		recipientIn.setRecipientPhoneNumber("9856746545");
		recipientIn.setMessage("uday here is is your gift from pallavi");

		cartResponseDto = new CartResponseDto();
		cartResponseDto.setCartId(1);
		cartResponseDto.setProductName("coffee mug");
		cartResponseDto.setQuantity(5);
		cartResponseDto.setRecipientDetails(listOfrecipients);
		cartResponseDto.setUserId(1);
		cartResponseDto.setUserType("corporate");
		cartResponseDto.setStatus(630);
	}

	@Test
	public void updateCartDetails() {

		Mockito.when(cartRepository.findByCartId(Mockito.anyLong())).thenReturn(Optional.of(cart));
		Mockito.when(cartRepository.save(Mockito.any(Cart.class))).thenReturn(cart);
		Mockito.when(recipientRepository.findByRecipientId(Mockito.anyLong())).thenReturn(Optional.of(recipient));
		Mockito.when(recipientRepository.save(Mockito.any(Recipient.class))).thenReturn(recipientIn);
		EditResponseDto response = cartServiceImpl.updateCartDetails(cartEditRequestDto);
		Assert.assertEquals(editResponseDto.getStatus(), response.getStatus());
	}

	@Test
	public void cartDetails() {
		Mockito.when(cartRepository.findByUserId(Mockito.anyLong())).thenReturn(Optional.of(cart));
		Mockito.when(productRepository.findByProductId(Mockito.anyLong())).thenReturn(Optional.of(product));
		Mockito.when(recipientRepository.findByCartId(Mockito.anyLong())).thenReturn(recipientList);
		CartResponseDto response = cartServiceImpl.cartDetails(userId);
		Assert.assertEquals(cartResponseDto.getStatus(), response.getStatus());
	}
}
