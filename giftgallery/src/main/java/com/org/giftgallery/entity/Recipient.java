package com.org.giftgallery.entity;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
public class Recipient {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long recipientId;
	private String recipientName;
	private String recipientEmail;
	private String recipientPhoneNumber;
	private long cartId;
	private String message;

}
