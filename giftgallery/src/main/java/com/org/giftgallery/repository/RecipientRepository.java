package com.org.giftgallery.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.org.giftgallery.entity.Recipient;

@Repository
public interface RecipientRepository extends JpaRepository<Recipient, Long> {

	List<Recipient> findByCartId(long cartId);

	Optional<Recipient> findByRecipientId(long recipientId);

}
