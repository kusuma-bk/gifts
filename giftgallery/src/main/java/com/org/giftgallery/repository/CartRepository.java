package com.org.giftgallery.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.org.giftgallery.entity.Cart;

@Repository
public interface CartRepository extends JpaRepository<Cart, Long> {

	Optional<Cart> findByCartId(long cartId);

	Optional<Cart> findByUserId(long userId);
	
	//List<Cart> cartList = cartRepository.findByUserId(long userId);

}
