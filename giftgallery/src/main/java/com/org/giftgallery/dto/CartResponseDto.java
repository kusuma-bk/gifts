package com.org.giftgallery.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CartResponseDto {
	private long userId;
	private long cartId;
	private String productName;
	private int quantity;
	private String userType;
	private int status;

	private List<RecipientRequestListDto> recipientDetails;
}
