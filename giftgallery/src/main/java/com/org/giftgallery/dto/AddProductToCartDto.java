package com.org.giftgallery.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class AddProductToCartDto {
	private long userId;
	private long productId;
	private int quantity;
	private String userType;
	List<RecipientDto> recipientDtos;

}
