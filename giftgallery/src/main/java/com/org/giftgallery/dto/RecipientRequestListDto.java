package com.org.giftgallery.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter

public class RecipientRequestListDto {

	private String recipientName;
	private String recipientEmail;
	private String recipientPhoneNumber;
	private String message;

	private long recipientId;
}
