package com.org.giftgallery.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ProductResponseDto {
	private long productId;
	private String productName;
	private double pricePerProduct;
	private int quantity;
	private String productDescription;
}
