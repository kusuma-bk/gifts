package com.org.giftgallery.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.org.giftgallery.dto.ProductListResponseDto;
import com.org.giftgallery.exception.ProductException;
import com.org.giftgallery.service.ProductService;

@RequestMapping("/products")
@RestController
public class ProductController {
	@Autowired
	ProductService productService;

	@GetMapping("")
	public ResponseEntity<ProductListResponseDto> productsList() throws ProductException {
		ProductListResponseDto productResponseDtos = productService.productList();

		return new ResponseEntity<>(productResponseDtos, HttpStatus.ACCEPTED);
	}
}
