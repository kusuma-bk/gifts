package com.org.giftgallery.controller;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.org.giftgallery.dto.OrderResponseDto;
import com.org.giftgallery.exception.CustemerTypeExceptionForCorperator;
import com.org.giftgallery.exception.CustemerTypeExceptionForPersonal;
import com.org.giftgallery.exception.OrderListNotPresent;
import com.org.giftgallery.service.OrderService;

@RequestMapping("/orders")
@RestController
public class OrderController {
	@Autowired
	OrderService orderService;



	@PostMapping("/{cartId}")
	public ResponseEntity<OrderResponseDto> order(@PathVariable long cartId)
			throws CustemerTypeExceptionForPersonal, OrderListNotPresent, CustemerTypeExceptionForCorperator, MessagingException {
		
		return new ResponseEntity<>(orderService.order(cartId), HttpStatus.ACCEPTED);
	}

}
