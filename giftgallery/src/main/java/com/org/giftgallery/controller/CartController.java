package com.org.giftgallery.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.org.giftgallery.dto.AddCartResponseDto;
import com.org.giftgallery.dto.AddProductToCartDto;
import com.org.giftgallery.dto.CartEditRequestDto;
import com.org.giftgallery.dto.CartResponseDto;
import com.org.giftgallery.dto.EditResponseDto;
import com.org.giftgallery.exception.CartException;
import com.org.giftgallery.exception.CartIdNotPresent;
import com.org.giftgallery.exception.CorporateException;
import com.org.giftgallery.exception.PersonalException;
import com.org.giftgallery.exception.QuantityException;
import com.org.giftgallery.exception.UserExistException;
import com.org.giftgallery.service.CartService;

@RequestMapping("/carts")
@RestController
public class CartController {

	@Autowired
	CartService cartService;

	@PostMapping("")
	public ResponseEntity<AddCartResponseDto> addToCart(@RequestBody AddProductToCartDto addProductToCartDto)
			throws QuantityException, CartException, UserExistException, PersonalException, CorporateException {
		AddCartResponseDto response = cartService.addToCard(addProductToCartDto);
		return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
	}

	@GetMapping("{userId}")
	public ResponseEntity<CartResponseDto> cartDetails(@PathVariable("userId") long userId) throws CartIdNotPresent {
		return new ResponseEntity<>(cartService.cartDetails(userId), HttpStatus.OK);
	}

	@PutMapping()
	public ResponseEntity<EditResponseDto> updateCartDetails(@RequestBody CartEditRequestDto cartEditRequestDto) {
		return new ResponseEntity<>(cartService.updateCartDetails(cartEditRequestDto), HttpStatus.OK);
	}

}
