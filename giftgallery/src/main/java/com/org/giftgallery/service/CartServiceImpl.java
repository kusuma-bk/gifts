package com.org.giftgallery.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.org.giftgallery.dto.AddCartResponseDto;
import com.org.giftgallery.dto.AddProductToCartDto;
import com.org.giftgallery.dto.CartEditRequestDto;
import com.org.giftgallery.dto.CartResponseDto;
import com.org.giftgallery.dto.EditResponseDto;
import com.org.giftgallery.dto.RecipientDto;
import com.org.giftgallery.dto.RecipientRequestListDto;
import com.org.giftgallery.entity.Cart;
import com.org.giftgallery.entity.Product;
import com.org.giftgallery.entity.Recipient;
import com.org.giftgallery.entity.User;
import com.org.giftgallery.exception.CartException;
import com.org.giftgallery.exception.CorporateException;
import com.org.giftgallery.exception.PersonalException;
import com.org.giftgallery.exception.QuantityException;
import com.org.giftgallery.exception.UserExistException;
import com.org.giftgallery.repository.CartRepository;
import com.org.giftgallery.repository.ProductRepository;
import com.org.giftgallery.repository.RecipientRepository;
import com.org.giftgallery.repository.UserRepository;
import com.org.giftgallery.utility.ProductUtility;

@Service
public class CartServiceImpl implements CartService {
	@Autowired
	ProductRepository productRepositry;
	@Autowired
	CartRepository cartRepositry;
	@Autowired
	RecipientRepository recipientRepositry;
	@Autowired
	UserRepository userRepository;

	@Override
	public AddCartResponseDto addToCard(AddProductToCartDto addProductToCartDto)
			throws QuantityException, CartException, UserExistException, PersonalException, CorporateException {
		long userId = addProductToCartDto.getUserId();
		System.out.println("hello");
		User users = userRepository.findByUserId(userId);
		System.out.println("hello");
		if (ObjectUtils.isEmpty(users))
			throw new UserExistException(ProductUtility.USER_EXIST_ERROR);

		System.out.println("hello");
		String userType = addProductToCartDto.getUserType();
		long productId = addProductToCartDto.getProductId();
		Optional<Product> product = productRepositry.findByProductId(productId);
		int quantityIn = product.get().getQuantity();
		if (quantityIn < addProductToCartDto.getQuantity())
			throw new QuantityException(ProductUtility.QUANTITY_ERROR);
		Cart cart = new Cart();
		cart.setProductId(productId);
		cart.setQuantity(addProductToCartDto.getQuantity());
		cart.setUserId(userId);
		cart.setUserType(userType);
		Cart cartInn = cartRepositry.save(cart);
		if (ObjectUtils.isEmpty(cartInn.getCartId()))
			throw new CartException(ProductUtility.CART_ERROR);

		if (userType.equalsIgnoreCase("personal")) {
			List<RecipientDto> recipientDto = addProductToCartDto.getRecipientDtos();
			int count1 = recipientDto.size();
			for (RecipientDto recipientDto2 : recipientDto) {
				if (count1 > 3) {
					throw new PersonalException(ProductUtility.USER_TYPE_PERSONAL_ERROR);

				} else {
					Recipient recipient = new Recipient();
					BeanUtils.copyProperties(recipientDto2, recipient);
					recipient.setCartId(cartInn.getCartId());
					recipientRepositry.save(recipient);

				}

			}
		}

		if (userType.equalsIgnoreCase("corporate")) {
			List<RecipientDto> recipientDto = addProductToCartDto.getRecipientDtos();
			int size = recipientDto.size();
			for (RecipientDto recipientDto2 : recipientDto) {
				if (size < 5) {
					throw new CorporateException(ProductUtility.USER_TYPE_CORPORATE_ERROR);

				} else {
					Recipient recipient = new Recipient();
					BeanUtils.copyProperties(recipientDto2, recipient);
					recipient.setCartId(cartInn.getCartId());
					recipientRepositry.save(recipient);

				}

			}
		}
		AddCartResponseDto addCartResponseDto = new AddCartResponseDto();
		addCartResponseDto.setCartId(cartInn.getCartId());
		addCartResponseDto.setMessage(ProductUtility.CART_MESSAGE);
		addCartResponseDto.setStatuscode(ProductUtility.CART_ERROR_STATUS);
		return addCartResponseDto;
	}

	@Override
	public CartResponseDto cartDetails(long userId) {

		Optional<Cart> cartList = cartRepositry.findByUserId(userId);
		Optional<Product> productList = productRepositry.findByProductId(cartList.get().getProductId());
		CartResponseDto cartResponseDto = new CartResponseDto();
		cartResponseDto.setUserId(userId);
		cartResponseDto.setCartId(cartList.get().getCartId());
		cartResponseDto.setQuantity(cartList.get().getQuantity());
		cartResponseDto.setUserType(cartList.get().getUserType());
		cartResponseDto.setProductName(productList.get().getProductName());
		List<Recipient> recipientList = recipientRepositry.findByCartId(cartList.get().getCartId());
		List<RecipientRequestListDto> recipientRequestListDto = new ArrayList<RecipientRequestListDto>();
		for (Recipient recipient : recipientList) {
			RecipientRequestListDto recipientRequestListDtoIn = new RecipientRequestListDto();
			BeanUtils.copyProperties(recipient, recipientRequestListDtoIn);
			recipientRequestListDto.add(recipientRequestListDtoIn);

		}

		cartResponseDto.setRecipientDetails(recipientRequestListDto);
		cartResponseDto.setStatus(630);
		return cartResponseDto;
	}

	@Override
	public EditResponseDto updateCartDetails(CartEditRequestDto cartEditRequestDto) {

		Optional<Cart> cartList = cartRepositry.findByCartId(cartEditRequestDto.getCartId());
		if (cartList.isPresent()) {
			if (cartList.get().getQuantity() != 0)
				cartList.get().setQuantity(cartEditRequestDto.getQuantity());
			cartRepositry.save(cartList.get());
		}
		List<RecipientRequestListDto> recipientList = cartEditRequestDto.getRecipientDetails();
		for (RecipientRequestListDto recipientRequestListDto : recipientList) {

			Optional<Recipient> recipient = recipientRepositry
					.findByRecipientId(recipientRequestListDto.getRecipientId());

			if (recipient.get().getRecipientEmail() != null && !recipient.get().getRecipientEmail().isEmpty())
				recipient.get().setRecipientEmail(recipientRequestListDto.getRecipientEmail());

			if (recipient.get().getRecipientName() != null && !recipient.get().getRecipientName().isEmpty())
				recipient.get().setRecipientName(recipientRequestListDto.getRecipientName());

			if (recipient.get().getRecipientPhoneNumber() != null
					&& !recipient.get().getRecipientPhoneNumber().isEmpty())
				recipient.get().setRecipientPhoneNumber(recipientRequestListDto.getRecipientPhoneNumber());

			if (recipient.get().getMessage() != null && !recipient.get().getMessage().isEmpty())
				recipient.get().setMessage(recipientRequestListDto.getMessage());

			recipientRepositry.save(recipient.get());
		}
		EditResponseDto editResponceDto = new EditResponseDto();
		editResponceDto.setMessage("updated success");
		editResponceDto.setStatus(680);
		return editResponceDto;
	}
}
