package com.org.giftgallery.service;

import com.org.giftgallery.dto.AddCartResponseDto;
import com.org.giftgallery.dto.AddProductToCartDto;
import com.org.giftgallery.dto.CartEditRequestDto;
import com.org.giftgallery.dto.CartResponseDto;
import com.org.giftgallery.dto.EditResponseDto;
import com.org.giftgallery.exception.CartException;
import com.org.giftgallery.exception.CartIdNotPresent;
import com.org.giftgallery.exception.CorporateException;
import com.org.giftgallery.exception.PersonalException;
import com.org.giftgallery.exception.QuantityException;
import com.org.giftgallery.exception.UserExistException;

public interface CartService {
	public AddCartResponseDto addToCard(AddProductToCartDto addProductToCartDto)
			throws QuantityException, CartException, UserExistException, PersonalException, CorporateException;

	public CartResponseDto cartDetails(long userId) throws CartIdNotPresent;

	public EditResponseDto updateCartDetails(CartEditRequestDto cartEditRequestDto);
}
