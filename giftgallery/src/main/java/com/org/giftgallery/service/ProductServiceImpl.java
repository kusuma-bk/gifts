package com.org.giftgallery.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.org.giftgallery.dto.ProductListResponseDto;
import com.org.giftgallery.dto.ProductResponseDto;
import com.org.giftgallery.entity.Product;
import com.org.giftgallery.exception.ProductException;
import com.org.giftgallery.repository.ProductRepository;
import com.org.giftgallery.utility.ProductUtility;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	ProductRepository productRepositry;

	@Override
	public ProductListResponseDto productList() throws ProductException {
		List<Product> products = productRepositry.findAll();

		if (ObjectUtils.isEmpty(products))
			throw new ProductException(ProductUtility.PRODUCTS_LIST_ERROR);
		ProductListResponseDto productListResponseDto = new ProductListResponseDto();
		List<ProductResponseDto> listOfProductResponseDto = new ArrayList<>();
		products.stream().map(product -> {
			ProductResponseDto productResponseDto = new ProductResponseDto();
			BeanUtils.copyProperties(product, productResponseDto);
			listOfProductResponseDto.add(productResponseDto);
			return productResponseDto;

		}).collect(Collectors.toList());
		productListResponseDto.setProductResponseDtos(listOfProductResponseDto);
		productListResponseDto.setStatusCode(ProductUtility.PRODUCTS_LIST__STATUS);
		return productListResponseDto;
	}

}
