package com.org.giftgallery.service;

import com.org.giftgallery.dto.ProductListResponseDto;
import com.org.giftgallery.exception.ProductException;

public interface ProductService {

	public ProductListResponseDto productList() throws ProductException;

}
