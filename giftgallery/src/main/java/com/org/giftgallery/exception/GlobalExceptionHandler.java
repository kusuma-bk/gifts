package com.org.giftgallery.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.org.giftgallery.utility.CustomerUtility;
import com.org.giftgallery.utility.ProductUtility;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
	@ExceptionHandler(CartException.class)
	public ResponseEntity<ErrorResponse> productsErrorException(CartException ex) {
		ErrorResponse errorResponse = new ErrorResponse();

		errorResponse.setMessage(ex.getMessage());
		errorResponse.setStatus(ProductUtility.CART_ERROR_STATUS);

		return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(QuantityException.class)
	public ResponseEntity<ErrorResponse> productsErrorException(QuantityException ex) {
		ErrorResponse errorResponse = new ErrorResponse();

		errorResponse.setMessage(ex.getMessage());
		errorResponse.setStatus(ProductUtility.QUANTITY_ERROR_STATUS);

		return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(ProductException.class)
	public ResponseEntity<ErrorResponse> productsErrorException(ProductException ex) {
		ErrorResponse errorResponse = new ErrorResponse();

		errorResponse.setMessage(ex.getMessage());
		errorResponse.setStatus(ProductUtility.PRODUCTS_LIST_ERROR_STATUS);

		return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(OrderListNotPresent.class)
	public ResponseEntity<ErrorResponse> customerErrorException(OrderListNotPresent ex) {
		ErrorResponse errorResponse = new ErrorResponse();

		errorResponse.setMessage(ex.getMessage());
		errorResponse.setStatus(CustomerUtility.ORDER_NOT_FOUNT_STATUS);

		return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(CustemerTypeExceptionForPersonal.class)
	public ResponseEntity<ErrorResponse> customerErrorException(CustemerTypeExceptionForPersonal ex) {
		ErrorResponse errorResponse = new ErrorResponse();

		errorResponse.setMessage(ex.getMessage());
		errorResponse.setStatus(CustomerUtility.CUSTOMER_TYPE_EXCEPTION_FOR_PERSONAL_STATUS);

		return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(CustemerTypeExceptionForCorperator.class)
	public ResponseEntity<ErrorResponse> customerErrorException(CustemerTypeExceptionForCorperator ex) {
		ErrorResponse errorResponse = new ErrorResponse();

		errorResponse.setMessage(ex.getMessage());
		errorResponse.setStatus(CustomerUtility.CUSTOMER_TYPE_EXCEPTION_FOR_PERSONAL_CORPERATE_STATUS);

		return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(UserExistException.class)
	public ResponseEntity<ErrorResponse> productsErrorException(UserExistException ex) {
		ErrorResponse errorResponse = new ErrorResponse();

		errorResponse.setMessage(ex.getMessage());
		errorResponse.setStatus(ProductUtility.USER_EXIST_ERROR_STATUS);

		return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(PersonalException.class)
	public ResponseEntity<ErrorResponse> productsErrorException(PersonalException ex) {
		ErrorResponse errorResponse = new ErrorResponse();

		errorResponse.setMessage(ex.getMessage());
		errorResponse.setStatus(ProductUtility.USER_TYPE_PERSONAL_STATUS);

		return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(CorporateException.class)
	public ResponseEntity<ErrorResponse> productsErrorException(CorporateException ex) {
		ErrorResponse errorResponse = new ErrorResponse();

		errorResponse.setMessage(ex.getMessage());
		errorResponse.setStatus(ProductUtility.USER_TYPE_CORPORATE_STATUS);

		return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
	}

}
