package com.org.giftgallery.exception;

public class CorporateException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5918173375418671210L;

	public CorporateException(String message) {
		super(message);
	}

}
