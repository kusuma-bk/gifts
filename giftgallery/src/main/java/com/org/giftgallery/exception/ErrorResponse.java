package com.org.giftgallery.exception;

import java.util.List;



public class ErrorResponse {
	private String message;
	private int status;

	private List<String> details;

	public ErrorResponse() {
		super();
	}

	public ErrorResponse(String message, List<String> details2, int status) {
		this.message = message;
		this.details = details2;
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public List<String> getDetails() {
		return details;
	}

	public void setDetails(List<String> details) {
		this.details = details;
	}

}
