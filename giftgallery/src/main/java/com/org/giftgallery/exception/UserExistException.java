package com.org.giftgallery.exception;

public class UserExistException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5918173375418671210L;

	public UserExistException(String message) {
		super(message);
	}

}
