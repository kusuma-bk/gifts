package com.org.giftgallery.utility;

public class CustomerUtility {
	public static final String ORDER_NOT_FOUNT = "your not having any details in cart";
	public static final int ORDER_NOT_FOUNT_STATUS = 605;

	public static final String CUSTOMER_TYPE_EXCEPTION_FOR_PERSONAL = "Your recipient count not more than 3 ";
	public static final int CUSTOMER_TYPE_EXCEPTION_FOR_PERSONAL_STATUS = 604;

	public static final String CUSTOMER_TYPE_EXCEPTION_FOR_CORPERATE = "Your recipient count must more than 5 ";
	public static final int CUSTOMER_TYPE_EXCEPTION_FOR_PERSONAL_CORPERATE_STATUS = 603;
}
